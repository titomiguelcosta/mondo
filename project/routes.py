from flask import request
from client import MondoClient
from campaign import Campaign


class Route(object):
    app = None

    def boot(self, app):
        self.app = app
        app.add_url_rule('/', 'index', self.index)
        app.add_url_rule('/transaction', 'transaction', self.transaction, methods=['POST', 'GET'])

    def index(self):
        return 'Hello from routes'

    def transaction(self):
        print(request)
        print(request.get_data())

        c = MondoClient()
        r = c.token(
            self.app.config['MONDO_API_CLIENT_ID'],
            self.app.config['MONDO_API_CLIENT_SECRET'],
            self.app.config['MONDO_API_USERNAME'],
            self.app.config['MONDO_API_PASSWORD']
        )
        a = c.accounts(r.json()["access_token"])
        body = request.get_json(force=True)
        t = c.transaction(body["data"]["id"], r.json()["access_token"], True)

        campaings = Campaign()
        number_campaigns, percentage_campaigns = campaings.campaigns(
            name=t.json()["transaction"]["description"],
            merchant=t.json()["transaction"]["merchant"]["group_id"],
            amount=t.json()["transaction"]["amount"]
        )

        if number_campaigns.count() > 0 or percentage_campaigns.count() > 0:
            if number_campaigns.count() > 0:
                for campaign in number_campaigns:
                    start_date = campaign.start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
                    end_date = campaign.end_date.strftime("%Y-%m-%dT%H:%M:%SZ")
                    t = c.transactions(
                        r.json()["access_token"],
                        a.json()["accounts"][0]["id"],
                        limit=1000,
                        since=start_date,
                        before=end_date
                    )
                    total = 0
                    merchant_name = None
                    for transaction in t.json()["transactions"]:
                        if transaction["merchant"]:
                            d = c.transaction(transaction["id"], r.json()["access_token"], True)
                            if campaign.match(d.json()["transaction"]["merchant"]["group_id"]):
                                total += 1
                                merchant_name = d.json()["transaction"]["merchant"]["name"]

                    if merchant_name:
                        if total == campaign.number:
                            f = c.create_feed_item(
                                r.json()["access_token"],
                                a.json()["accounts"][0]["id"],
                                "Congratulations: you got refunded %.2f at %s" % (
                                    min(campaign.limit, abs(float(transaction["amount"])/100)),
                                    merchant_name
                                ),
                                "http://www.affairscloud.com/wp-content/uploads/2015/06/zj42kn2xzlivgllfpa2u1.jpg"
                            )
                            print(f.json())
                            return "Number campaign"
                        elif total > 0:
                            f = c.create_feed_item(
                                r.json()["access_token"],
                                a.json()["accounts"][0]["id"],
                                "Almost there: you need %d more purchases at %s to get a refund" % (
                                    campaign.number - total,
                                    merchant_name
                                ),
                                "http://www.affairscloud.com/wp-content/uploads/2015/06/zj42kn2xzlivgllfpa2u1.jpg"
                            )
                            print(f.json())

            if percentage_campaigns.count() > 0:
                for campaign in percentage_campaigns:
                    start_date = campaign.start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
                    end_date = campaign.end_date.strftime("%Y-%m-%dT%H:%M:%SZ")
                    t = c.transactions(
                        r.json()["access_token"],
                        a.json()["accounts"][0]["id"],
                        limit=1000,
                        since=start_date,
                        before=end_date
                    )
                    for transaction in t.json()["transactions"]:
                        if transaction["merchant"]:
                            d = c.transaction(transaction["id"], r.json()["access_token"], True)
                            amount = abs(float(transaction["amount"]) / 100)
                            if campaign.match(d.json()["transaction"]["merchant"]["group_id"], amount):
                                f = c.create_feed_item(
                                    r.json()["access_token"],
                                    a.json()["accounts"][0]["id"],
                                    "Congratulations: you got a discount of %d%% at %s" % (
                                        campaign.discount,
                                        d.json()["transaction"]["merchant"]["name"]
                                    ),
                                    "http://www.affairscloud.com/wp-content/uploads/2015/06/zj42kn2xzlivgllfpa2u1.jpg"
                                )
                                print(f.json())
                                return "Percentage campaign"
                            elif d.json()["transaction"]["merchant"]["group_id"] == campaign.merchant and campaign.limit > amount:
                                f = c.create_feed_item(
                                    r.json()["access_token"],
                                    a.json()["accounts"][0]["id"],
                                    "Almost there: you had to spend at least %.2f£ at %s to get a discount of %d%%" % (
                                        campaign.limit,
                                        d.json()["transaction"]["merchant"]["name"],
                                        campaign.discount
                                    ),
                                    "http://www.affairscloud.com/wp-content/uploads/2015/06/zj42kn2xzlivgllfpa2u1.jpg"
                                )
                                print(f.json())

        return "No match"
