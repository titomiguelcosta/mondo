from sqlalchemy import and_
import datetime
from models import NumberCampaign, DiscountCampaign

class Campaign(object):
    def campaigns(self, name, merchant, amount):
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        number_campaigns = NumberCampaign.query.filter(
            and_(
                NumberCampaign.start_date <= now,
                NumberCampaign.end_date >= now,
                NumberCampaign.enabled == True,
                NumberCampaign.merchant == merchant
            )
        )

        discount_campaigns = DiscountCampaign.query.filter(
            and_(
                NumberCampaign.start_date <= now,
                NumberCampaign.end_date >= now,
                NumberCampaign.enabled == True,
                NumberCampaign.merchant == merchant
            )
        )

        return number_campaigns, discount_campaigns