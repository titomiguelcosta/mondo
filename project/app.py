from flask import Flask
from routes import Route
from config import Config
from administration import Administration
from models import db

app = Flask(__name__)
Config().boot(app)
Route().boot(app)

db.init_app(app)

admin = Administration(app, db)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
