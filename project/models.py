from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class NumberCampaign(db.Model):
    """
    After a certain number of transactions, last purchase is free
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    merchant = db.Column(db.String(255))
    number = db.Column(db.Integer)
    limit = db.Column(db.Integer)
    start_date = db.Column(db.DateTime)
    end_date = db.Column(db.DateTime)
    enabled = db.Column(db.Boolean)

    def match(self, merchant):
        return merchant == self.merchant

    def __repr__(self):
        return '<NumberCampaign %r>' % self.name


class DiscountCampaign(db.Model):
    """
    Every time you buy something at the store you get a discount
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    merchant = db.Column(db.String(255))
    discount = db.Column(db.Integer)
    limit = db.Column(db.Integer)
    start_date = db.Column(db.DateTime)
    end_date = db.Column(db.DateTime)
    enabled = db.Column(db.Boolean)

    def match(self, merchant, amount):
        return merchant == self.merchant and amount >= self.limit

    def __repr__(self):
        return '<DiscountCampaign %r>' % self.name
