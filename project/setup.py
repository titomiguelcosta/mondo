from models import db, NumberCampaign, DiscountCampaign
from config import Config
from flask import Flask
import datetime

app = Flask(__name__)
Config().boot(app)

db.app = app
db.init_app(app)

db.create_all()
starbucks = NumberCampaign(name='starbucks6for5', merchant='grp_00008yEdJuHtW2yOx17n3B', number=6, limit=5, start_date=datetime.datetime(2015, 1, 1, 0, 0, 0), end_date=datetime.datetime(2015, 12, 30, 23, 59, 59), enabled=True)
pret = DiscountCampaign(name='pret10off', merchant='grp_00008yEdAfaWnaPaksR52f', discount=10, limit=2, start_date=datetime.datetime(2015, 1, 1, 0, 0, 0), end_date=datetime.datetime(2015, 12, 30, 23, 59, 59), enabled=True)
mcds = NumberCampaign(name='mcds3for2', merchant='grp_00008yEdJyMeMJ9bavNe1B', number=3, limit=5, start_date=datetime.datetime(2015, 1, 1, 0, 0, 0), end_date=datetime.datetime(2015, 12, 30, 23, 59, 59), enabled=False)

db.session.add(starbucks)
db.session.add(pret)
db.session.add(mcds)

db.session.commit()
