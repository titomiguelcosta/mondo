from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from models import NumberCampaign, DiscountCampaign

class Administration(object):
    def __init__(self, app, db):
        self.app = app
        self.db = db
        self.admin = Admin(app, name='mondo', template_mode='bootstrap3')
        self.add_view(NumberCampaign)
        self.add_view(DiscountCampaign)

    def add_view(self, model):
        self.admin.add_view(ModelView(model, self.db.session))
