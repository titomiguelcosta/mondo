# Cookbook Name:: Mondo
# Recipe:: default
#
# Copyright (C) 2015 YOUR_NAME
#
# All rights reserved - Do Not Redistribute

# make sure no questions are asked during the instalation process
ENV["DEBIAN_FRONTEND"] = "noninteractive"

execute "apt-get -y update"
execute "apt-get -y upgrade"
execute "apt-get -y autoremove"
execute "apt-get install -y nginx"
execute "apt-get install -y mysql-server"
execute "apt-get install -y supervisor"
execute "apt-get install -y git"
execute "apt-get install -y python3-pip"
execute "pip3 install gunicorn"
execute "pip3 install flask"
execute "pip3 install flask-admin"
execute "pip3 install flask-sqlalchemy"
execute "python3 /project/mondo/mondo.py &"
